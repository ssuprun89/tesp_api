from django.urls import path

from main_page.views import Index, ParseData

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('parse-data/', ParseData.as_view(), name='parse_data')
]
