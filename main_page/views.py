from django.shortcuts import redirect
from django.views.generic import TemplateView, View

from stories.tasks import parser_task


class Index(TemplateView):
    template_name = 'index.html'


class ParseData(View):

    @staticmethod
    def post(request, *args, **kwargs):
        parser_task.delay(request.POST['stories'])
        return redirect('index')
