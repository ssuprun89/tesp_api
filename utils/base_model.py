import datetime

from django.db import models


class WatchDogModel(models.Model):
    updated_at = models.DateTimeField(verbose_name="Updated at", auto_now=True)
    created_at = models.DateTimeField(verbose_name="Created at", auto_now_add=True)

    class Meta:
        abstract = True


class BaseModelData(WatchDogModel):
    by = models.CharField(max_length=250)
    type = models.CharField(max_length=20)
    time = models.DateTimeField()
    title = models.TextField()
    text = models.TextField()
    score = models.IntegerField()
    kids = models.TextField()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        print(2)
        self.kids = ','.join(map(str, self.kids))
        self.time = datetime.datetime.utcfromtimestamp(self.time)
        return super().save(force_insert=False, force_update=False, using=None, update_fields=None)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class DefaultModel(models.Model):
    class Meta:
        abstract = True
        verbose_name = 'Default Model'
