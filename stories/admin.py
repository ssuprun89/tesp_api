from django.contrib import admin

from stories.models import StoryPost, JobPost

admin.site.register(StoryPost)
admin.site.register(JobPost)
