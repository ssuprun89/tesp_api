from django.db import models

from utils.base_model import BaseModelData

'''
    BASE FIELD

    by = models.CharField(max_length=250)
    type = models.CharField(max_length=20)
    time = models.TimeField()
    title = models.TextField()
    text = models.TextField()
    score = models.IntegerField()
    kids = models.TextField()
    
'''


class StoryPost(BaseModelData):
    descendants = models.IntegerField()


class JobPost(BaseModelData):
    url = models.URLField()
