import datetime
from abc import ABC, abstractmethod
from typing import Union

import requests

from stories.models import StoryPost, JobPost
from utils.base_model import DefaultModel


class BaseStoriesWorker(ABC):
    __BASE_URL = 'https://hacker-news.firebaseio.com/'
    __VERSION = 'v0'

    @property
    @abstractmethod
    def TYPE_STORIES(self) -> str:
        return ''

    @property
    @abstractmethod
    def MODEL(self):
        return DefaultModel

    def __init__(self):
        self.url = f'{self.__BASE_URL}{self.__VERSION}'

    @staticmethod
    def send_request(url: str, method_: str = 'GET') -> Union[dict, list]:
        req = requests.get(url, method_)
        response = req.json()
        return response

    def _get_list(self) -> list:
        url = f'{self.url}/{self.TYPE_STORIES}.json'
        list_id = self.send_request(url)
        return list_id

    def _get_exist_ids(self) -> list:
        return self.MODEL.objects.all().values_list('id', flat=True)

    def _get_data_by_id(self, id_) -> dict:
        url = f'{self.url}/item/{id_}.json'
        data = self.send_request(url)
        return data

    def _prepare_data(self, id_: int) -> dict:
        data = self._get_data_by_id(id_)
        data['kids'] = ','.join(map(str, data.get('kids', [])))
        data['time'] = datetime.datetime.utcfromtimestamp(data['time'])
        return data

    def parse(self):
        list_id = self._get_list()
        exists_id = self._get_exist_ids()
        # print(datetime.datetime.now())
        list_obj_to_save = [self.MODEL(**self._prepare_data(id_)) for id_ in list_id if id_ not in exists_id]
        self.MODEL.objects.bulk_create(list_obj_to_save)
        # print(datetime.datetime.now())


class AskStoriesWorker(BaseStoriesWorker):
    TYPE_STORIES = 'askstories'
    MODEL = StoryPost


class ShowStoriesWorker(BaseStoriesWorker):
    TYPE_STORIES = 'showstories'
    MODEL = StoryPost


class NewStoriesWorker(BaseStoriesWorker):
    TYPE_STORIES = 'newstories'
    MODEL = StoryPost


class JobStoriesWorker(BaseStoriesWorker):
    TYPE_STORIES = 'jobstories'
    MODEL = JobPost
