from stories.worker import (
    ShowStoriesWorker,
    JobStoriesWorker,
    AskStoriesWorker,
    NewStoriesWorker,
)
from tesp_api.celery import app


@app.task
def parser_task(type_stories: str):
    workers_obj = {
        'show': ShowStoriesWorker,
        'new': NewStoriesWorker,
        'ask': AskStoriesWorker,
        'job': JobStoriesWorker,
    }
    worker = workers_obj[type_stories]()
    worker.parse()
